# Ejemplos Básicos de Virtual Bank

Se tienen diferentes casos de uso para que los desarrolladores se guien en el uso de esta herramienta

## ¿Cómo funciona?

Siga los siguientes pasos:

#### Login a Registry Docker de BBVA
```sh
docker login -u <user.artifactory> -p <apikey.artifactory> globaldevtools.bbva.com:5000
```

#### 1.- Descargar este repositorio
```Shell
git clone ssh://git@globaldevtools.bbva.com:7999/dev_ops/vbank_examples.git
```
Considerar la estructura de carpetas:

    .
    ├── ...
    ├── vbank                    # Estructura para levantar virtual bank
    │   ├── contracts            # Archivos para casos host - tx
    │   ├── data                 # Archivos 'response' para protocolos http/soap
    │   ├── stubs                # Archivos '.yml' para stubs de pruebas
    ├── docker-compose.yml       # Archivo Docker para iniciar el ambiente virtualizado
    └── ...


#### 2.- Iniciar el stack Virtual Bank
```Shell
docker-compose up -d
```

#### 3.- Probar los diferentes casos de uso
```Shell
curl -X GET 'http://localhost:7050/api/dummy/prueba?parametro1=dato1&parametro2=dato2'
```

###### Ejemplo de un stub
Cuando se consume la URL: http://localhost:7050/api/dummy/prueba2 con los siguientes datos:
```json
{
    "codigo": "2",
    "mensaje": "OK",
    "data": {
        "campo1": "2",
        "campo2": "valor2"
    },
    "idUsuario": "P023666",
    "nombre": "bean_usuario"
}
```

```Shell
curl -i -X POST -H 'custom: custom2' -H 'Content-Type: application/json' -d '{"codigo":"2","mensaje":"OK","data":{"campo1":"2","campo2":"valor2"},"idUsuario":"P023666","nombre":"bean_usuario"}' http://localhost:7050/api/dummy/prueba2
```

El stub se comportará según lo definido: el nodo "campo1 debe ser valor 2 y el header debe ser "custom2" con valor "custom"
```yml
when:
  backend: restful
  method: POST
  path: /api/dummy/prueba2
  match:
    body.data.campo1: '2'
    headers.custom: 'custom2'

then:
- return:
    :headers:
      content-type: application/xml
      custom: $input.headers.custom
    :body:
          HelloResponse:
            code: $input.body.codigo
            description: $input.body.mensaje
            data:
              :attr.nombre: campo1
              :attr.valor: $input.body.data.campo1
              valor: myvalue1
```

Entonces, sólo así, se obtendrá una respuesta satisfactoria de vbank.
```xml
<HelloResponse>
    <code>2</code>
    <data nombre="campo1" valor="2">
        <valor>myvalue1</valor>
    </data>
    <description>OK</description>
</HelloResponse>
```